This repo is a clone of the wger docker repo found here:
https://github.com/wger-project/docker

See the original wger code here: https://github.com/wger-project/wger

This repo has slight edits to match my docker, traefik, and future kubernetes setup.
For example, I removed nginx from the equation in favor of traefik.
Still, the same license applies as was found on the original repo.
Future changes are to link this to my postgresql and redis docker containers instead
of having its own.
